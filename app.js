const express = require('express')
const multer = require('multer')
const ejs = require('ejs')
const path = require('path')

// set storage engine
const storage = multer.diskStorage({
	destination: './public/uploads/',
	filename: (req, file, callback) => {
		callback(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`)
	}
})

// init upload
const upload = multer({
	storage: storage
}).single('photo')

// init app
const app = express()
const PORT = 3000;

// EJS
app.set('view engine', 'ejs')

// public folder
app.use(express.static('./public'))

app.get('', (req, res) => res.render('index'))

app.post('/upload', (req, res) => {
	upload(req, res, (err) => {
		if (err) {
			res.render('index', {
				msg: err
			})
		} else {
			console.log(req.file)
			res.send('test')
		}
	})
})

app.listen(PORT, () => console.log(`Server running on port ${PORT}`))